function shiftFn(ken) {
    var shift = document.getElementById("shiftBtn");
    var arr = document.getElementsByTagName("td");


    if (ken == 1) {
        shift.setAttribute("onclick", "shiftFn(0)");
        shift.style.backgroundColor = "#FF4500";
        arr[2].innerHTML = "sin<sup>-1</sup>";
        arr[2].setAttribute("onclick", "trigo1('sin')");
        arr[3].innerHTML = "cos<sup>-1</sup>";
        arr[3].setAttribute("onclick", "trigo1('cos')");
        arr[4].innerHTML = "tan<sup>-1</sup>";
        arr[4].setAttribute("onclick", "trigo1('tan')");
        arr[10].innerHTML = "ln";
        arr[10].setAttribute("onclick", "log(0)");
        arr[28].innerHTML = "\u0065";
        arr[28].setAttribute("onclick", "piOrE('e')");
      } else {
        shift.setAttribute("onclick", "shiftFn(1)");
        shift.style.backgroundColor = "#CD853F";
        arr[2].innerHTML = "sin";
        arr[2].setAttribute("onclick", "trigo('sin')");
        arr[3].innerHTML = "cos";
        arr[3].setAttribute("onclick", "trigo('cos')");
        arr[4].innerHTML = "tan";
        arr[4].setAttribute("onclick", "trigo('tan')");
        arr[10].innerHTML = "log";
        arr[10].setAttribute("onclick", "log(1)");
        arr[28].innerHTML = "\u03C0";
        arr[28].setAttribute("onclick", "piOrE('pi')");
        }
}


//función suma
function input(sun) {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    a.value += sun;
    b.innerHTML += sun;
}



function factorial(shirious) {
    if (Number.isInteger(shirious)) {
        if (shirious < 2) return 1;
        return shirious * factorial(shirious - 1);
    }
}

function sqrt() {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    a.value += "sqrt(";
    b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
        " * Math.sqrt(" : "Math.sqrt(";
}

function leftParen() {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    a.value += "(";
    b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
        " * (" : "(";
}

function piOrE(lunar) {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    if (lunar == "pi") {
        a.value += "\u03C0";
        b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
            " * Math.PI" : "Math.PI";
    } else {
        a.value += "\u0065";
        b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
            " * Math.E" : "Math.E";
    }
}

function log(jafca) {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    if (jafca == 1) {
        a.value += "log(";
        b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
            " * Math.log10(" : "Math.log10(";
    } else {
        a.value += "ln(";
        b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
            " * Math.log(" : "Math.log(";
    }
}

function trigo(hatsyrei) {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    a.value += hatsyrei + "(";
    b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
        " * Math." + hatsyrei + "(Math.PI / 180 * " : "Math." + hatsyrei + "(Math.PI / 180 * ";
}

function trigo1(valentin) {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    a.value += valentin + "\u207B\u00B9("
    b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
        " * 180 / Math.PI * Math.a" + valentin + "(" : "180 / Math.PI * Math.a" + valentin + "(";
}

function multOrDiv(edward) {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    if (edward == "mult") {
        a.value += "\u00D7";
        b.innerHTML += "*";
    } else {
        a.value += "\u00F7";
        b.innerHTML += "/"
    }
}

function del() {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    var c = document.getElementById("myAns");
    if (a.value.slice(-3) == "Ans") {
        b.innerHTML = (/[\d)IE]/.test(a.value.slice(-4, -3))) ?
            b.innerHTML.slice(0, -(c.innerHTML.length + 3)) : b.innerHTML.slice(0, -(c.innerHTML.length));
        a.value = a.value.slice(0, -3);
    } else if (a.value == "Error!") {
        ac();
    } else {
        switch (b.innerHTML.slice(-2)) {
            case "* ": // sin cos tan
                b.innerHTML = (/[\d)IE]/.test(a.value.slice(-5, -4))) ?
                    b.innerHTML.slice(0, -28) : b.innerHTML.slice(0, -25);
                a.value = a.value.slice(0, -4);
                break;
            case "n(":
            case "s(": // csin ccos ctan
                b.innerHTML = (/[\d)IE]/.test(a.value.slice(-7, -6))) ?
                    b.innerHTML.slice(0, -29) : b.innerHTML.slice(0, -26);
                a.value = a.value.slice(0, -6);
                break;
            case "0(": // log
                b.innerHTML = (/[\d)IE]/.test(a.value.slice(-5, -4))) ?
                    b.innerHTML.slice(0, -14) : b.innerHTML.slice(0, -11);
                a.value = a.value.slice(0, -4);
                break;
            case "g(": // ln
                b.innerHTML = (/[\d)IE]/.test(a.value.slice(-4, -3))) ?
                    b.innerHTML.slice(0, -12) : b.innerHTML.slice(0, -9);
                a.value = a.value.slice(0, -3);
                break;
            case "t(": // sqrt
                b.innerHTML = (/[\d)IE]/.test(a.value.slice(-6, -5))) ?
                    b.innerHTML.slice(0, -13) : b.innerHTML.slice(0, -10);
                a.value = a.value.slice(0, -5);
                break;
            case "PI": // pi
                b.innerHTML = (/[\d)IE]/.test(a.value.slice(-2, -1))) ?
                    b.innerHTML.slice(0, -10) : b.innerHTML.slice(0, -7);
                a.value = a.value.slice(0, -1);
                break;
            case ".E": // e
                b.innerHTML = (/[\d)IE]/.test(a.value.slice(-2, -1))) ?
                    b.innerHTML.slice(0, -9) : b.innerHTML.slice(0, -6);
                a.value = a.value.slice(0, -1);
                break;
            default:
                b.innerHTML = b.innerHTML.slice(0, -1);
                a.value = a.value.slice(0, -1);
        };
    }
}

function ac() {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    a.value = b.innerHTML = "";
}

function ans() {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    var c = document.getElementById("myAns");
    a.value += "Ans";
    b.innerHTML += (/[\d)IE]/.test(b.innerHTML.slice(-1))) ?
        " * " + c.innerHTML : c.innerHTML;
}





function equal() {
    var a = document.getElementById("result");
    var b = document.getElementById("myPara");
    var c = document.getElementById("myAns");
    for (var i = 0; i < a.value.split("(").length - a.value.split(")").length; i++) {
        b.innerHTML += ")";
    }
    if (b.innerHTML != "") {
        a.value = b.innerHTML = c.innerHTML = eval(b.innerHTML
            .replace(/(\d+\.?\d*)\!/g, "factorial($1)")
            .replace(/(\(?[^(]*\)?)\^(\(?.*\)?)/, "Math.pow($1, $2)")
        );
    }
    if (!isFinite(a.value)) a.value = "Error!";
}